FROM alpine:3.17

RUN apk update && apk add --no-cache \
    nodejs \
    npm \
    git

WORKDIR /usr/app

COPY package*.json ./

RUN npm install --cache /usr/local/npm_cache

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]